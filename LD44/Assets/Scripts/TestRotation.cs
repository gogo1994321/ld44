using UnityEngine;

[ExecuteInEditMode]
public class TestRotation : MonoBehaviour
{
    public Vector3 rotation;

    void Update()
    {
        transform.localRotation = Quaternion.Euler(rotation);
    }
}