﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killzone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Damageable d = other.GetComponent<Damageable>();
        if (d)
        {
            var msg = new Damageable.DamageMessage();
            msg.amount = 10000;
            d.ApplyDamage(msg);
        }
    }
}
