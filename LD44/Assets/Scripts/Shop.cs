﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour, IInteract
{
    public ShopSettings settings;
    public ShopUI shopUI;
    public GameObject shopHoveringIcon;
    public float hoverHeight = 3f;
    GameObject shopHoveringIconInstance;

    InventoryItem playerVialItems;

    int currentVialAmount;
    int currentAmmoAmount;

    PlayerInventory playerInventory;
    Transform cam;

    private void Start()
    {
        shopUI.exitButton.onClick.AddListener(() => CloseUI());

        shopUI.vialUp.onClick.AddListener(() => IncreaseVialAmount());
        shopUI.vialDown.onClick.AddListener(() => DecreaseVialAmount());
        shopUI.vialMaxUp.onClick.AddListener(() => SetToMaxVial());
        shopUI.vialSell.onClick.AddListener(() => SellVial());

        shopUI.ammoUp.onClick.AddListener(() => IncreaseAmmoAmount());
        shopUI.ammoAll.onClick.AddListener(() => MaxAmmo());
        shopUI.ammoDown.onClick.AddListener(() => DecreaseAmmoAmount());
        shopUI.buyAmmo.onClick.AddListener(() => BuyAmmo());

        shopUI.buyHealth.onClick.AddListener(() => BuyMaxHealth());
        shopUI.buyCrossbowUp.onClick.AddListener(() => BuyCrossbowUpgrade());

        shopUI.sellHealthButton.onClick.AddListener(() => SellHealthButton());

        cam = FindObjectOfType<Camera>().transform;
        shopHoveringIconInstance = Instantiate(shopHoveringIcon, transform.position + Vector3.up * hoverHeight, Quaternion.LookRotation(-cam.forward, Vector3.up));
        shopHoveringIconInstance.transform.SetParent(GameObject.FindGameObjectWithTag("3DCanvas").transform);
        shopHoveringIconInstance.GetComponent<FloatUI>().Setup(transform.position + -transform.forward * 3.2f, hoverHeight);
    }

    private void Update()
    {
        shopHoveringIconInstance.transform.rotation = Quaternion.LookRotation(-cam.forward, Vector3.up);

        if (!SO_Catalog.instance.gameState.playerInUI)
        {
            shopUI.gameObject.SetActive(false);
        }
    }

    public void Interact(GameObject Player)
    {
        SO_Catalog.instance.gameState.playerInUI = true;
        if (!playerInventory)
            playerInventory = Player.GetComponent<PlayerInventory>();

        playerVialItems = playerInventory.GetItem("bloodvial");
        shopUI.gameObject.SetActive(true);
        UpdateTextDisplays();
    }

    void CloseUI()
    {
        SO_Catalog.instance.gameState.playerInUI = false;
        shopUI.gameObject.SetActive(false);
    }

    void UpdateTextDisplays()
    {
        shopUI.ammoAmount.text = currentAmmoAmount.ToString();
        shopUI.vialAmountDisplay.text = currentVialAmount.ToString();

        shopUI.moneyDisplay.text = SO_Catalog.instance.playerSettings.money.ToString();
        shopUI.ammoCost.text = settings.costOfAmmo.ToString() + "x";
        shopUI.maxHealthUpCost.text = settings.costOfMaxHealth.ToString();
        shopUI.crossbowUpCost.text = settings.costOfWeaponUpgrade.ToString();
    }

    void IncreaseVialAmount()
    {
        if (playerVialItems)
        {
            currentVialAmount = Mathf.Min(currentVialAmount + 1, playerVialItems.settings.amountInStack);
            UpdateTextDisplays();
        }
    }
    void DecreaseVialAmount()
    {
        currentVialAmount = Mathf.Max(currentAmmoAmount - 1, 0);
        UpdateTextDisplays();
    }
    void SetToMaxVial()
    {
        if (playerVialItems)
        {
            currentVialAmount = playerVialItems.settings.amountInStack;
            UpdateTextDisplays();
        }
    }
    void SellVial()
    {
        if (playerVialItems)
        {
            SO_Catalog.instance.playerSettings.money += currentVialAmount * playerVialItems.GetComponent<Bloodvial>().bloodValue;
            for (int i = 0; i < currentVialAmount; i++)
            {
                playerInventory.RemoveItem(playerVialItems, true);
            }
            currentVialAmount = 0;
            UpdateTextDisplays();
            PlayBuySound();
        }
    }

    void IncreaseAmmoAmount()
    {
        if (SO_Catalog.instance.playerSettings.money >= (currentAmmoAmount + 1) * settings.costOfAmmo)
        {
            currentAmmoAmount = currentAmmoAmount + 1;
            UpdateTextDisplays();
        }
    }
    void MaxAmmo()
    {
        currentAmmoAmount = SO_Catalog.instance.playerSettings.money / settings.costOfAmmo;
        UpdateTextDisplays();
    }
    void DecreaseAmmoAmount()
    {
        currentAmmoAmount = Mathf.Max(currentAmmoAmount - 1, 0);
        UpdateTextDisplays();
    }
    void BuyAmmo()
    {

        SO_Catalog.instance.playerSettings.ammo += currentAmmoAmount;
        SO_Catalog.instance.playerSettings.money -= currentAmmoAmount * settings.costOfAmmo;
        currentAmmoAmount = 0;
        UpdateTextDisplays();
        PlayBuySound();

    }

    void BuyMaxHealth()
    {
        if (SO_Catalog.instance.playerSettings.money >= settings.costOfMaxHealth)
        {
            SO_Catalog.instance.playerSettings.AddMaxHealth(settings.amountOfMaxHealth);
            Debug.Log("New current health:" + SO_Catalog.instance.playerSettings.damageable.currentHitPoints);
            SO_Catalog.instance.playerSettings.money -= settings.costOfMaxHealth;
            settings.costOfMaxHealth = Mathf.RoundToInt(1.5f * settings.costOfMaxHealth);
            UpdateTextDisplays();
            PlayBuySound();
        }
    }
    void BuyCrossbowUpgrade()
    {
        if (SO_Catalog.instance.playerSettings.money >= settings.costOfWeaponUpgrade)
        {
            WeaponSettings wps = playerInventory.GetItem("crossbow").settings as WeaponSettings;
            wps.currentDamage += settings.amountOfWeaponUpgrade;
            SO_Catalog.instance.playerSettings.money -= settings.costOfWeaponUpgrade;
            settings.costOfWeaponUpgrade = Mathf.RoundToInt(1.25f * settings.costOfWeaponUpgrade);
            UpdateTextDisplays();
            PlayBuySound();
        }
    }

    void SellHealthButton()
    {
        if (SO_Catalog.instance.playerSettings.damageable.currentHitPoints > settings.damageForHealth)
        {
            SO_Catalog.instance.playerSettings.ApplyDamage(15);
            SO_Catalog.instance.playerSettings.money += settings.coinsForHealth;
        }
    }

    void PlayBuySound()
    {
        GetComponent<AudioSource>().PlayOneShot(settings.buySound);
    }
}
