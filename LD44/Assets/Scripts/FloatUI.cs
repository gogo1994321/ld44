using UnityEngine;

public class FloatUI : MonoBehaviour
{

    public Vector2 hoverDifference;
    public float hoverSpeed = 1f;

    Vector3 hoverOver;
    float startHeight;
    float hoverT;
    bool goingUp;

    public void Setup(Vector3 hoverOver, float startHeight)
    {
        this.hoverOver = hoverOver;
        this.startHeight = startHeight;
    }

    private void Update()
    {
        if (hoverT >= 1f)
        {
            goingUp = false;
        }
        if (hoverT <= 0f)
        {
            goingUp = true;
        }
        hoverT = goingUp ? hoverT + (Time.deltaTime * hoverSpeed) : hoverT - (Time.deltaTime * hoverSpeed);

        transform.position = new Vector3(hoverOver.x, Mathf.SmoothStep(startHeight - hoverDifference.y, startHeight + hoverDifference.x, hoverT), hoverOver.z);
    }
}