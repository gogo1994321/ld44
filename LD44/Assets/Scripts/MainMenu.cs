﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    float waitBetweenAnims = 0f;
    public Animator anim;
    public void StartButton()
    {
        SceneManager.LoadScene(1);
    }
    public void ExitButton()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (waitBetweenAnims < 10f)
        {
            waitBetweenAnims += Time.deltaTime;
        }
        else
        {
            int random = Random.Range(0, 3);
            if (random == 0)
            {
                anim.SetInteger("attackIndex", Random.Range(0, 6));
                anim.SetTrigger("attackMelee");
            }
            if (random == 1)
            {
                anim.SetTrigger("smallJump");
            }
            if (random == 2)
            {
                anim.SetTrigger("spotted");
            }
            waitBetweenAnims = 0;
        }
    }
}
