﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(BoxCollider))]
public class TriggerPlayerEntered : MonoBehaviour
{
    [SerializeField]
    UnityEvent triggerEnter;
    [SerializeField]
    UnityEvent triggerExit;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            triggerEnter.Invoke();
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            triggerExit.Invoke();
    }
}
