using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopUI : MonoBehaviour
{
    public TMP_Text moneyDisplay;
    public TMP_Text vialAmountDisplay;
    public TMP_Text maxHealthUpCost;
    public TMP_Text ammoCost;
    public TMP_Text ammoAmount;
    public TMP_Text crossbowUpCost;

    public Button vialUp;
    public Button vialDown;
    public Button vialMaxUp;
    public Button vialSell;

    public Button exitButton;

    public Button ammoUp;
    public Button ammoAll;
    public Button ammoDown;
    public Button buyAmmo;

    public Button buyHealth;
    public Button buyCrossbowUp;

    public Button sellHealthButton;
}