﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteract
{
    void Interact(GameObject Player);
}

public class PlayerInteraction : MonoBehaviour
{

    Transform playerCamera;
    PlayerUI playerUI;

    // Use this for initialization
    void Start()
    {
        playerCamera = Camera.main.transform;
        playerUI = GetComponent<PlayerUI>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = new Ray(playerCamera.position, playerCamera.forward); ;
        if (Physics.Raycast(ray, out hit, SO_Catalog.instance.playerSettings.interactDistance))
        {
            IInteract interactComponent = hit.transform.GetComponent<IInteract>();
            Transform currentHit = hit.transform;
            Transform hitParent = hit.transform.parent;
            while (hitParent != null && interactComponent == null)
            {
                interactComponent = hitParent.transform.GetComponent<IInteract>();
                currentHit = hitParent;
                hitParent = hitParent.parent;
            }
            if (Input.GetButtonDown("Interact") && SO_Catalog.instance.playerSettings.canInteract)
            {
                interactComponent?.Interact(gameObject);
            }
        }
    }
}
