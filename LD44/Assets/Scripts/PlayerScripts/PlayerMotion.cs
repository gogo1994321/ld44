﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotion : MonoBehaviour
{
    [SerializeField]
    private Camera cam;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;

    [SerializeField]
    private float cameraRotationLimit = 85f;

    private Rigidbody rb;
    private float distanceToFloor;
    private bool inAir;
    float waitBeforeGravity;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        distanceToFloor = GetComponent<Collider>().bounds.extents.y + 0.1f;
        waitBeforeGravity = SO_Catalog.instance.playerSettings.waitBeforeExtraGravity;
    }

    // Run every physics iteration
    void FixedUpdate()
    {
        if (!SO_Catalog.instance.gameState.paused)
        {
            PerformMovement();
            PerformRotation();
        }

        if (inAir)
        {
            if (!Physics.Raycast(transform.position, Vector3.down, distanceToFloor))
            {
                if (waitBeforeGravity > 0)
                {
                    waitBeforeGravity -= Time.deltaTime;
                }
                else
                {
                    rb.AddForce(Physics.gravity * SO_Catalog.instance.playerSettings.afterJumpGravityForce, ForceMode.Acceleration);
                }
            }
            else if (waitBeforeGravity <= 0)
            {
                inAir = false;
                waitBeforeGravity = SO_Catalog.instance.playerSettings.waitBeforeExtraGravity;
            }
        }
    }

    // Gets a movement vector
    public void Move(Vector3 _velocity)
    {
        if (!inAir || (inAir && _velocity != Vector3.zero))
            velocity = _velocity;
    }

    // Gets a rotational vector
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }

    // Gets a rotational vector for the camera
    public void RotateCamera(float _cameraRotationX)
    {
        cameraRotationX = _cameraRotationX;
    }


    //Perform movement based on velocity variable
    void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
    }


    //Perform rotation
    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null)
        {
            // Set our rotation and clamp it
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

            //Apply our rotation to the transform of our camera
            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
        }
    }

    public void Jump()
    {
        if (Physics.Raycast(transform.position, Vector3.down, distanceToFloor))
        {
            inAir = true;
            rb.AddForce(Vector3.up * Mathf.Sqrt(SO_Catalog.instance.playerSettings.jumpHeight * -2f * Physics.gravity.y), ForceMode.VelocityChange);
        }
    }

}
