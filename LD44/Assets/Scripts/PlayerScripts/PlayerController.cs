﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float lookSensitivity = 3f;

    // Component caching
    private PlayerMotion playerMotion;

    #region Singleton
    private static PlayerController _instance;

    public static PlayerController instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PlayerController>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }
    #endregion

    void Start()
    {
        playerMotion = GetComponent<PlayerMotion>();
    }

    void Update()
    {
        if (SO_Catalog.instance.gameState.paused || SO_Catalog.instance.gameState.playerInUI)
        {
            if (Cursor.lockState != CursorLockMode.None)
                Cursor.lockState = CursorLockMode.None;

            Cursor.visible = true;
            playerMotion.Move(Vector3.zero);
            playerMotion.Rotate(Vector3.zero);
            playerMotion.RotateCamera(0f);

            return;
        }

        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        //Jump
        if (Input.GetButtonDown("Jump"))
        {
            playerMotion.Jump();
        }

        //Calculate movement velocity as a 3D vector
        float _xMov = Input.GetAxisRaw("Horizontal");
        float _zMov = Input.GetAxisRaw("Vertical");

        float _sprintMultiplier = Input.GetButton("Run") ? SO_Catalog.instance.playerSettings.sprintMultiplier : 1.0f;

        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVertical = transform.forward * _zMov;

        // Final movement vector
        Vector3 _velocity = (_movHorizontal + _movVertical).normalized * SO_Catalog.instance.playerSettings.speed * _sprintMultiplier;

        //Apply movement
        playerMotion.Move(_velocity);

        //Calculate rotation as a 3D vector (turning around)
        float _yRot = Input.GetAxisRaw("Mouse X");

        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * lookSensitivity;

        //Apply rotation
        playerMotion.Rotate(_rotation);

        //Calculate camera rotation as a 3D vector (turning around)
        float _xRot = Input.GetAxisRaw("Mouse Y");

        float _cameraRotationX = _xRot * lookSensitivity;

        //Apply camera rotation
        playerMotion.RotateCamera(_cameraRotationX);

    }
}
