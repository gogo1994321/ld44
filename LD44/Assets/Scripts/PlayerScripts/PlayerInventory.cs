﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    [SerializeField]
    int inventorySize = 10;

    [SerializeField]
    Transform handPosition;

    InventoryItem selectedItem;

    List<InventoryItem> inventory = new List<InventoryItem>();

    //UI
    [SerializeField]
    Transform uiParent;

    [SerializeField]
    GameObject itemUIPrefab;

    List<GameObject> existingItemUIs = new List<GameObject>();

    public void AddItem(InventoryItem item)
    {
        if (inventory.Count < inventorySize)
        {
            if (item.settings.stackable)
            {
                InventoryItem existingItem = inventory.Find(x => x == item);
                if (existingItem)
                {
                    existingItem.settings.amountInStack++;
                    Destroy(item.gameObject);
                    return;
                }
            }

            inventory.Add(item);
            SetupItem(true, item);
            item.doWhenDestroyed.AddListener(() => RemoveItem(item));
            SelectItem(inventory.Count - 1);

            if (itemUIPrefab != null && uiParent != null)
            {
                GameObject newUI = Instantiate(itemUIPrefab, Vector3.zero, Quaternion.identity);
                newUI.transform.SetParent(uiParent);
                newUI.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().sprite = item.settings.itemIcon;
                newUI.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => SelectItem(existingItemUIs.Count));
                existingItemUIs.Add(newUI);
            }
        }
    }

    void SetupItem(bool added, InventoryItem item)
    {
        if (added)
        {
            item.transform.position = handPosition.position;
            if (item.settings.inHandRotation == Vector3.zero)
            {
                item.transform.rotation = handPosition.rotation;
            }
            else
            {
                item.transform.localRotation = Quaternion.Euler(item.settings.inHandRotation);
            }
        }
        item.transform.SetParent(added ? handPosition : null);

        Rigidbody rb = item.GetComponent<Rigidbody>();
        if (rb)
            rb.isKinematic = added;

        item.GetComponentInChildren<Collider>().enabled = !added;

    }

    public void RemoveItem(InventoryItem item, bool deleteRightAway = false)
    {
        if (item.settings.stackable && item.settings.amountInStack > 1)
        {
            item.settings.amountInStack--;
        }
        else
        {
            if (deleteRightAway)
            {
                Destroy(item.gameObject);
            }
            else
            {
                SetupItem(false, item);
            }

            if (existingItemUIs.Count > 0)
            {
                GameObject uiItem = existingItemUIs[inventory.IndexOf(item)];
                existingItemUIs.Remove(uiItem);
                Destroy(uiItem);
            }
            inventory.Remove(item);
            SelectItem(0);
            selectedItem = null;
        }
    }

    void SelectItem(int itemIndex)
    {
        selectedItem?.gameObject.SetActive(false);
        if (inventory.Count > 0)
        {
            selectedItem = inventory[(itemIndex) % inventory.Count];
            selectedItem.gameObject.SetActive(true);
        }
    }

    public InventoryItem GetItem(string typeID)
    {
        return inventory.Find(x => x.settings.typeID == typeID);
    }

    void Update()
    {
        if (!SO_Catalog.instance.gameState.paused && !SO_Catalog.instance.gameState.playerInUI)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {

                int index = selectedItem != null ? inventory.IndexOf(selectedItem) : 0;
                SelectItem(index + 1);
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {

                int index = selectedItem != null ? inventory.IndexOf(selectedItem) : 0;
                SelectItem(index - 1 < 0 ? inventory.Count - 1 : index - 1);

            }
            if (Input.anyKeyDown)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (Input.GetKeyDown("" + i) && inventory.Count > i)
                    {
                        SelectItem(i);
                    }
                }
            }
            if (Input.GetButtonDown("LMB"))
            {
                selectedItem?.Use();
            }
            if (Input.GetButtonDown("Inventory"))
            {
                uiParent.gameObject.SetActive(!uiParent.gameObject.activeSelf);
            }
        }
    }
}
