﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public GameObject deathScreen;
    Damageable dmg;

    void Start()
    {
        dmg = GetComponent<Damageable>();
        SO_Catalog.instance.playerSettings.damageable = dmg;
        SO_Catalog.instance.playerSettings.Reset();
        dmg.OnDeath.AddListener(() => Dead());
        dmg.OnReceiveDamage.AddListener(() => SO_Catalog.instance.gameState.playerInUI = false);
        deathScreen.GetComponent<Image>().CrossFadeAlpha(0f, 0f, true);
        SO_Catalog.instance.gameState.paused = false;
    }

    void Dead()
    {
        SO_Catalog.instance.gameState.paused = true;
        deathScreen.SetActive(true);
        deathScreen.GetComponent<Image>().CrossFadeAlpha(1f, 2f, false);
    }
}
