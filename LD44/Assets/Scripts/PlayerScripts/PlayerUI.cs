﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUI : MonoBehaviour
{

    public Image healthBar;
    public TMP_Text healthValue;
    public TMP_Text ammoValue;

    Damageable playerHealth;

    private void Start()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Damageable>();
    }


    private void Update()
    {
        healthValue.text = playerHealth.currentHitPoints.ToString();
        healthBar.fillAmount = playerHealth.currentHitPoints * 1f / SO_Catalog.instance.playerSettings.currentMaxHealth;

        ammoValue.text = "Ammo: " + SO_Catalog.instance.playerSettings.ammo.ToString();
    }

    /* 
        public List<Sprite> cursors;
        public Image uiCursor;

        public void SetCursor(int i, Vector2 size)
        {
            if (cursors.Count > i && i >= 0)
            {
                uiCursor.sprite = cursors[i];
            }
        }*/
}
