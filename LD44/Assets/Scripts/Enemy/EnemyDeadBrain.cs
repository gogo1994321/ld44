using System.Collections;
using UnityEngine;

public class EnemyDeadBrain : MonoBehaviour, IInteract
{
    [HideInInspector]
    public EnemySettings settings;

    bool isHarvested;
    [HideInInspector]
    public float currentBloodValue;

    [SerializeField]
    EnemyUIDisplay uiDisplay;

    Camera cam;

    private void Start()
    {
        currentBloodValue = settings.bloodValue;
        cam = FindObjectOfType<Camera>();
    }
    private void Update()
    {
        if (!isHarvested)
        {
            currentBloodValue = Mathf.Max(0, currentBloodValue - settings.bloodDrainSpeed * Time.deltaTime);
            uiDisplay.Update(currentBloodValue / settings.bloodValue, transform.position, Quaternion.LookRotation(-cam.transform.forward, Vector3.up));
        }
        if (currentBloodValue <= 0)
        {
            Destroy(uiDisplay.GetUI());
            Destroy(this.gameObject);
        }
    }

    public void Interact(GameObject Player)
    {
        if (!isHarvested)
        {
            isHarvested = true;
            GameObject vial = Instantiate(settings.bloodVialPrefab, transform.position, Quaternion.identity);
            Player.GetComponent<PlayerInventory>().AddItem(vial.GetComponent<Bloodvial>());
            Bloodvial commonVial = Player.GetComponent<PlayerInventory>().GetItem("bloodvial").gameObject.GetComponent<Bloodvial>();
            commonVial.bloodValue += Mathf.RoundToInt(currentBloodValue);
            GetComponent<AudioSource>().PlayOneShot(settings.bloodVialPickupSound);
            Destroy(uiDisplay.GetUI());
            StartCoroutine(DeleteBody());
        }
    }

    IEnumerator DeleteBody()
    {
        float t = 10f;
        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        Destroy(this.gameObject);
    }
}