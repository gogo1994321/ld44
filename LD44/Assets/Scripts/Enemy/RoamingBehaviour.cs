using UnityEngine;
using UnityEngine.AI;

class RoamingBehaviour : BehaviourBase
{
    float elapsedTimeSinceMoving;
    Vector3 spawnPos;
    float roamRadius;
    float waitBetweenMoves;

    public RoamingBehaviour(Vector3 spawnPos, NavMeshAgent agent, EnemySettings settings) : base(settings, agent)
    {
        this.spawnPos = spawnPos;
        this.roamRadius = settings.roamRadius;
        this.agent = agent;
        this.waitBetweenMoves = settings.waitBetweenRoams;
    }

    public override void Tick()
    {
        if (elapsedTimeSinceMoving > waitBetweenMoves)
        {
            elapsedTimeSinceMoving = 0;
            Vector3 target = spawnPos;
            bool foundTarget = false;
            while (!foundTarget)
            {
                foundTarget = RandomPoint(spawnPos, roamRadius);
            }
            
        }
        elapsedTimeSinceMoving += Time.deltaTime;
    }

    bool RandomPoint(Vector3 center, float range)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * range;
        return SetDestination(randomPoint);
    }
}