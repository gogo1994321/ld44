using UnityEngine.AI;

abstract class BehaviourBase
{
    protected NavMeshAgent agent;
    protected EnemySettings settings;
    public BehaviourBase(EnemySettings settings, NavMeshAgent agent)
    {
        this.agent = agent;
        this.settings = settings;
    }
    public abstract void Tick();

    protected bool SetDestination(UnityEngine.Vector3 destination)
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(destination, out hit, 1.0f, NavMesh.AllAreas))
        {
            agent.destination = hit.position;
            return true;
        }
        return false;
    }
}