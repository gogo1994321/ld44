using UnityEngine;
using UnityEngine.AI;

class ConvergingBehaviour : BehaviourBase
{
    public Transform target;
    public ConvergingBehaviour(EnemySettings settings, NavMeshAgent agent) : base(settings, agent)
    {
    }

    public override void Tick()
    {
        SetDestination(target.position);
    }
}