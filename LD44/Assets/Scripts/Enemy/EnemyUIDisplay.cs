using UnityEngine;
[System.Serializable]
public class EnemyUIDisplay
{
    public GameObject bloodDisplayPrefab;
    public float bloodDisplayDelta;
    GameObject bloodDisplayInstance;

    FloatUI floatUI;

    public void Update(float value, Vector3 pos, Quaternion rot)
    {
        if (!bloodDisplayInstance)
        {
            bloodDisplayInstance = GameObject.Instantiate(bloodDisplayPrefab, pos, rot);
            bloodDisplayInstance.transform.SetParent(GameObject.FindGameObjectWithTag("3DCanvas").transform);
            floatUI = bloodDisplayInstance.GetComponent<FloatUI>();
        }
        floatUI.Setup(pos, bloodDisplayDelta);
        bloodDisplayInstance.transform.rotation = rot;

        bloodDisplayInstance.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().fillAmount = value;
    }

    public GameObject GetUI()
    {
        return bloodDisplayInstance;
    }
}