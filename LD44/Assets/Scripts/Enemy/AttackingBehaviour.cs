using UnityEngine;
using UnityEngine.AI;

class AttackingBehaviour : BehaviourBase
{
    Transform target;
    Animator animator;
    float currentJumpWait = 0;
    bool canAttack;
    public AttackingBehaviour(Transform target, NavMeshAgent agent, EnemySettings settings, Animator animator) : base(settings, agent)
    {
        this.target = target;
        this.animator = animator;
    }

    public override void Tick()
    {
        SetDestination(target.position);
        float distanceToTarget = (target.position - agent.gameObject.transform.position).magnitude;
        if (distanceToTarget <= settings.meleeAttackDistance && canAttack)
        {
            if (animator.GetCurrentAnimatorStateInfo(1).IsName("Wait") || animator.GetCurrentAnimatorStateInfo(1).IsName("AttackStart"))
                Attack();
        }
        else if (distanceToTarget < settings.smallJumpDistance && currentJumpWait >= settings.waitBetweenJumps)
        {
            if (Random.Range(0f, 1f) < settings.chanceToJump)
            {
                animator.SetTrigger("smallJump");
            }
            currentJumpWait = 0;
        }
        else if (distanceToTarget < settings.bigJumpDistance && currentJumpWait >= settings.waitBetweenJumps)
        {
            if (Random.Range(0f, 1f) < settings.chanceToJump)
            {
                animator.SetTrigger("bigJump");
            }
            currentJumpWait = 0;
        }

        if (currentJumpWait < settings.waitBetweenJumps)
        {
            currentJumpWait += Time.deltaTime;
        }

        canAttack = animator.GetCurrentAnimatorStateInfo(2).IsName("Wait");
    }

    void Attack()
    {
        if (animator.GetNextAnimatorStateInfo(1).IsName("Wait") || animator.GetNextAnimatorStateInfo(1).IsName("AttackStart"))
        {
            animator.SetInteger("attackMeleeIndex", Random.Range(0, 6));
        }

        animator.SetTrigger("attackMelee");
    }
}