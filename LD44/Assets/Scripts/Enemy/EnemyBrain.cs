using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBrain : MonoBehaviour
{
    public EnemySettings settings;
    public List<AudioClip> snarls;
    public AudioSource snarlSource;
    public Vector2 timeBetweenSnarls;
    public List<AudioClip> steps;
    public AudioSource footSource;
    public AudioClip spottedRoar;
    [HideInInspector]
    public bool forceAttack;
    public enum EnemyState
    {
        Roaming,
        Converge,
        Attacking
    }

    EnemyState state;
    GameObject player;

    RoamingBehaviour roamingBehaviour;
    ConvergingBehaviour convergeBehaviour;
    AttackingBehaviour attackingBehaviour;

    BehaviourBase currentBehaviour;

    [SerializeField]
    TargetScanner playerScanner;

    protected PlayerController m_Target = null;
    protected float m_TimerSinceLostTarget = 0.0f;

    Damageable damagable;

    List<EnemyBrain> allies;

    float snarlTimer;
    float currentWait;
    AudioSource aSource;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        NavMeshAgent agent = GetComponent<NavMeshAgent>();

        roamingBehaviour = new RoamingBehaviour(transform.position, agent, settings);
        convergeBehaviour = new ConvergingBehaviour(settings, agent);
        attackingBehaviour = new AttackingBehaviour(player.transform, agent, settings, GetComponent<Animator>());

        damagable = GetComponent<Damageable>();
        SetMaxHealth();
        damagable.OnDeath.AddListener(() => Died());
        damagable.OnReceiveDamage.AddListener(() =>
        {
            if (state != EnemyState.Attacking)
            {
                m_Target = player.GetComponent<PlayerController>();
            }
        });

        allies = FindObjectsOfType<EnemyBrain>().ToList();
        allies.Remove(this);

        aSource = GetComponent<AudioSource>();
    }

    public void SetMaxHealth(int modifier = 0)
    {
        if (!damagable)
            damagable = GetComponent<Damageable>();

        damagable.maxHitPoints = settings.maxHealth + modifier;
        damagable.ResetDamage();
    }

    private void Update()
    {
        if (SO_Catalog.instance.gameState.playerInUI)
        {
            return;
        }
        switch (state)
        {
            case EnemyState.Roaming:

                currentBehaviour = roamingBehaviour;
                break;

            case EnemyState.Converge:

                currentBehaviour = convergeBehaviour;
                break;

            case EnemyState.Attacking:

                currentBehaviour = attackingBehaviour;
                break;
        }


        FindTarget();

        //Transition to states
        if (m_Target && (state == EnemyState.Converge || state == EnemyState.Roaming))
        {
            if (state == EnemyState.Roaming)
            {
                int max = Mathf.Min(allies.Count, 2);
                for (int i = 0; i < max; i++)
                {
                    if (allies[i].state == EnemyState.Roaming)
                    {
                        allies[i].SetConvergeTarget(transform);
                        allies[i].SetState(EnemyState.Converge);
                    }
                }
            }
            SetState(EnemyState.Attacking);
            GetComponent<Animator>().SetTrigger("spotted");
        }

        if (state == EnemyState.Attacking && !m_Target && !forceAttack)
        {
            state = EnemyState.Roaming;
        }
        currentBehaviour.Tick();

        if (snarlTimer >= currentWait)
        {
            currentWait = Random.Range(timeBetweenSnarls.x, timeBetweenSnarls.y);
            snarlSource.PlayOneShot(snarls[Random.Range(0, snarls.Count)]);
            snarlSource.pitch = Random.Range(0.70f, 1.25f);
            snarlTimer = 0;
        }
        else
        {
            snarlTimer += Time.deltaTime;
        }

    }

    void Died()
    {
        GameObject ragdoll = GetComponent<ReplaceWithRagdoll>().Replace(false);
        ragdoll.GetComponent<EnemyDeadBrain>().settings = settings;
        Destroy(this.gameObject);
    }

    public void SetConvergeTarget(Transform target)
    {
        convergeBehaviour.target = target;
    }

    public void SetState(EnemyState state)
    {
        this.state = state;
    }

    public void FindTarget()
    {
        //we ignore height difference if the target was already seen
        PlayerController target = playerScanner.Detect(transform, m_Target == null);

        if (m_Target == null)
        {
            //we just saw the player for the first time, pick an empty spot to target around them
            if (target != null)
            {
                //m_Controller.animator.SetTrigger(hashSpotted);
                m_Target = target;
            }
        }
        else
        {
            //we lost the target. But werewolfes have a special behaviour : they only loose the player scent if they move past their detection range
            //and they didn't see the player for a given time. Not if they move out of their detectionAngle. So we check that this is the case before removing the target
            if (target == null)
            {
                m_TimerSinceLostTarget += Time.deltaTime;

                if (m_TimerSinceLostTarget >= settings.timeToStopPursuit)
                {
                    Vector3 toTarget = m_Target.transform.position - transform.position;

                    if (toTarget.sqrMagnitude > playerScanner.detectionRadius * playerScanner.detectionRadius)
                    {
                        //the target move out of range, reset the target
                        m_Target = null;
                    }
                }
            }
            else
            {
                if (target != m_Target)
                {
                    m_Target = target;
                }
                m_TimerSinceLostTarget = 0.0f;
            }
        }
    }

    public void DoDamage()
    {
        Vector3 toPlayer = player.transform.position - transform.position;
        if (Vector3.Angle(transform.forward, toPlayer) < 90f && toPlayer.magnitude < 2.5f)
        {
            SO_Catalog.instance.playerSettings.ApplyDamage(settings.attackDamage);
        }
        else
        {
            //Debug.Log("Angle was: " + Vector3.Angle(transform.forward, toPlayer) + " distance was: " + toPlayer.magnitude);
        }
    }

    public void PlayStep()
    {
        footSource.PlayOneShot(steps[Random.Range(0, steps.Count)]);
        footSource.pitch = Random.Range(0.70f, 1.25f);
    }

    public void SpottedRoar()
    {
        aSource.PlayOneShot(spottedRoar);
    }
}