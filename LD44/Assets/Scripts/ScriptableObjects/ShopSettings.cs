using UnityEngine;

[CreateAssetMenu(fileName = "ShopSettings", menuName = "ScriptableObject/ShopSettings", order = 7)]
public class ShopSettings : ScriptableObject
{
    public int costOfMaxHealth;
    public int amountOfMaxHealth;
    public int costOfAmmo;
    public int costOfWeaponUpgrade;
    public int amountOfWeaponUpgrade;
    public int coinsForHealth;
    public int damageForHealth;
    public AudioClip buySound;
}