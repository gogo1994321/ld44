using UnityEngine;

[CreateAssetMenu(fileName = "EnemySettings", menuName = "ScriptableObject/EnemySettings", order = 2)]
public class EnemySettings : ScriptableObject
{
    [Header("Roaming")]
    public float roamRadius;
    public float waitBetweenRoams;
    [Header("Health")]
    public int maxHealth;
    [Header("Blood")]
    public GameObject bloodVialPrefab;
    public float bloodValue;
    public float bloodDrainSpeed;
    public AudioClip bloodVialPickupSound;
    [Header("Attacking")]
    public float timeToStopPursuit;
    public float meleeAttackDistance;
    public int attackDamage;
    [Space]
    [Range(0f, 1f)]
    public float chanceToJump;
    public float waitBetweenJumps;
    public float smallJumpDistance;
    public float bigJumpDistance;
}