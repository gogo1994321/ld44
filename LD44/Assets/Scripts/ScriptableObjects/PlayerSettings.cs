﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerSetting", menuName = "ScriptableObject/PlayerSetting", order = 1)]
public class PlayerSettings : ScriptableObject
{
    //Interaction
    [System.NonSerialized]
    public bool canInteract = true;
    public float interactDistance = 10f;

    //Movement
    public float speed = 5f;
    public float sprintMultiplier = 1.5f;
    public float jumpHeight;
    public float waitBeforeExtraGravity;
    public float afterJumpGravityForce;

    //Health
    public int maxHealth;
    [System.NonSerialized]
    public int currentMaxHealth;
    [System.NonSerialized]
    public Damageable damageable;

    //Shop stuff
    [System.NonSerialized]
    public int money;
    [System.NonSerialized]
    public int ammo;
    public int startAmmo;

    public void ApplyDamage(int dmg, bool audible = true)
    {
        var msg = new Damageable.DamageMessage();
        msg.amount = dmg;
        damageable.ApplyDamage(msg, audible);
    }

    public void AddMaxHealth(int health)
    {
        currentMaxHealth += health;
        damageable.maxHitPoints += health;
        damageable.ResetDamage();
    }

    public void Reset()
    {
        ammo = startAmmo;
        currentMaxHealth = maxHealth;
        damageable.maxHitPoints = currentMaxHealth;
        damageable.ResetDamage();
    }
}
