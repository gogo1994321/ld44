using UnityEngine;

[CreateAssetMenu(fileName = "GameState", menuName = "ScriptableObject/GameState", order = 0)]
public class GameState : ScriptableObject
{
    [System.NonSerialized]
    public bool paused;
    [System.NonSerialized]
    public bool playerInUI;
}