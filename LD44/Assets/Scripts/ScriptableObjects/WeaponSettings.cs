using UnityEngine;

[CreateAssetMenu(fileName = "WeaponSettings", menuName = "ScriptableObject/WeaponSettings", order = 4)]
public class WeaponSettings : InventoryItemSettings
{
    public float attackDistance;
    public float reloadSpeed;
    [System.NonSerialized]
    public int currentDamage;
    public int damage;

    
    public AudioClip reloadClip;
}