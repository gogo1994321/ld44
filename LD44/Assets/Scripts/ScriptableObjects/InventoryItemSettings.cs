using UnityEngine;

[CreateAssetMenu(fileName = "InventoryItemSettings", menuName = "ScriptableObject/InventoryItemSettings", order = 5)]
public class InventoryItemSettings : ScriptableObject
{
    public Sprite itemIcon;
    public AudioClip[] useClips;
    public Vector3 inHandRotation;
    public string typeID;
    public bool stackable;
    [System.NonSerialized]
    public int amountInStack = 1;

}