using UnityEngine;

public class Knife : InventoryItem
{
    private WeaponSettings castedSettings;

    bool didDamage;

    Transform cam;
    float reload;

    Animator animator;
    AudioSource source;

    private void Start()
    {
        castedSettings = settings as WeaponSettings;
        castedSettings.currentDamage = castedSettings.damage;
        animator = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
    }

    public override void Interact(GameObject Player)
    {
        base.Interact(Player);
        cam = Player.transform.GetComponentInChildren<Camera>().transform;
    }

    public override void Use()
    {
        if (reload >= castedSettings.reloadSpeed)
        {
            animator.SetInteger("attackIndex", Random.Range(0, 2));
            animator.SetTrigger("attack");
            RaycastHit hit;
            if (Physics.Raycast(cam.position, cam.forward, out hit, castedSettings.attackDistance))
            {
                Damageable damageable = hit.transform.GetComponent<Damageable>();
                if (damageable)
                {
                    var msg = new Damageable.DamageMessage();
                    msg.amount = castedSettings.currentDamage;
                    msg.damageSource = transform.position;
                    msg.damager = this;
                    damageable.ApplyDamage(msg);
                    didDamage = true;
                }
            }
            reload = 0;
        }
    }

    private void Update()
    {
        if (reload < castedSettings.reloadSpeed)
        {
            reload += Time.deltaTime;
        }
    }

    public void PlaySound()
    {
        if (!didDamage)
        {
            source.PlayOneShot(settings.useClips[Random.Range(0, settings.useClips.Length)]);
            source.pitch = Random.Range(0.75f, 1.5f);
        }
        else
        {
            didDamage = false;
        }
    }
}