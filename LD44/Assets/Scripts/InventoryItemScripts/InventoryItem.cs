﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InventoryItem : MonoBehaviour, IInteract, IEquatable<InventoryItem>
{
    public InventoryItemSettings settings;
    public UnityEvent doWhenPickedUp;
    public UnityEvent doWhenUsed;
    public UnityEvent doWhenDestroyed;


    public virtual void Interact(GameObject Player)
    {
        doWhenPickedUp?.Invoke();
        Player.GetComponent<PlayerInventory>().AddItem(this);
    }

    public void Destroy()
    {
        doWhenDestroyed?.Invoke();
        Destroy(this.gameObject);
    }

    public virtual void Use()
    {
        doWhenUsed?.Invoke();
    }

    #region  Equality Operators
    public override bool Equals(object obj)
    {
        return this.Equals(obj as InventoryItem);
    }

    public bool Equals(InventoryItem other)
    {
        // If parameter is null, return false.
        if (System.Object.ReferenceEquals(other, null))
        {
            return false;
        }

        // Optimization for a common success case.
        if (System.Object.ReferenceEquals(this, other))
        {
            return true;
        }

        // If run-time types are not exactly the same, return false.
        if (this.GetType() != other.GetType())
        {
            return false;
        }

        return settings.typeID == other.settings.typeID;
    }
    public override int GetHashCode()
    {
        return settings.typeID.GetHashCode() * 0x00010000 + settings.typeID.GetHashCode();
    }
    public static bool operator ==(InventoryItem lhs, InventoryItem rhs)
    {
        // Check for null on left side.
        if (System.Object.ReferenceEquals(lhs, null))
        {
            if (System.Object.ReferenceEquals(rhs, null))
            {
                // null == null = true.
                return true;
            }

            // Only the left side is null.
            return false;
        }
        // Equals handles case of null on right side.
        return lhs.Equals(rhs);
    }

    public static bool operator !=(InventoryItem lhs, InventoryItem rhs)
    {
        return !(lhs == rhs);
    }
    #endregion
}
