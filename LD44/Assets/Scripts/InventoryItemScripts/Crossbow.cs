﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossbow : InventoryItem
{
    public GameObject ammoPrefab;
    public float shootForce;
    public GameObject visualArrow;

    Transform cam;
    AudioSource audioSource;
    float reload;
    Animator animator;
    private WeaponSettings castedSettings;
    bool needReload;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        castedSettings = settings as WeaponSettings;
        animator = GetComponent<Animator>();
        castedSettings.currentDamage = castedSettings.damage;
    }

    private void Update()
    {
        if (reload < castedSettings.reloadSpeed)
        {
            reload += Time.deltaTime;
            if (reload >= castedSettings.reloadSpeed && needReload && SO_Catalog.instance.playerSettings.ammo > 0)
            {
                animator.SetTrigger("reload");
                audioSource.PlayOneShot(castedSettings.reloadClip);
                needReload = false;
                visualArrow.SetActive(true);
            }
        }
    }

    public override void Interact(GameObject Player)
    {
        base.Interact(Player);
        cam = Player.transform.GetComponentInChildren<Camera>().transform;
    }

    public override void Use()
    {
        if (reload >= castedSettings.reloadSpeed && SO_Catalog.instance.playerSettings.ammo > 0)
        {
            animator.SetTrigger("shoot");
            visualArrow.SetActive(false);
            GameObject GO = Instantiate(ammoPrefab, transform.position + transform.forward * 1f, transform.rotation);
            GO.GetComponent<Bolt>().damage = castedSettings.currentDamage;
            audioSource.PlayOneShot(castedSettings.useClips[0]);
            RaycastHit hit;
            Physics.Raycast(cam.position, cam.forward, out hit);
            Vector3 forceDir = hit.transform != null ? (hit.point - transform.position) : cam.transform.forward;
            GO.GetComponent<Rigidbody>().AddForce(forceDir.normalized * shootForce, ForceMode.Impulse);
            reload = 0;
            needReload = true;
            SO_Catalog.instance.playerSettings.ammo--;
        }
    }
}
