using UnityEngine;

public class Bolt : MonoBehaviour
{
    public int damage;
    public float timeBeforeCleanup = 10f;
    bool didDamage;

    private void OnCollisionEnter(Collision other)
    {
        if (!didDamage)
        {
            var damageable = other.gameObject.GetComponent<Damageable>();
            if (damageable)
            {
                var msg = new Damageable.DamageMessage();
                msg.amount = damage;
                msg.damageSource = transform.position;
                msg.damager = this;
                damageable.ApplyDamage(msg);
            }
            didDamage = true;
        }
    }

    private void Update()
    {
        if (timeBeforeCleanup > 0)
        {
            timeBeforeCleanup -= Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}