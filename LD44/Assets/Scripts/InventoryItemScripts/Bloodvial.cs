using System.Collections;
using UnityEngine;

public class Bloodvial : InventoryItem
{
    public int healValue;
    public int bloodValue;
    public override void Use()
    {
        SO_Catalog.instance.playerSettings.ApplyDamage(-healValue, false);
        GetComponent<AudioSource>().PlayOneShot(settings.useClips[0]);
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        float t = settings.useClips[0].length;

        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        FindObjectOfType<PlayerInventory>().RemoveItem(this, true);
    }
}