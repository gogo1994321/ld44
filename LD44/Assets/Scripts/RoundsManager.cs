using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoundsManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public TMPro.TMP_Text roundDisplay;
    public Image roundBarDisplay;
    public int startingAmount;
    public List<Transform> spawnLocations;
    public float waitBeforeNextRound;

    int roundCounter = 0;
    List<GameObject> spawnedEnemies = new List<GameObject>();
    float currentWait;
    AudioSource source;
    List<Transform> availableSpawns = new List<Transform>();

    private void Start()
    {
        source = GetComponent<AudioSource>();
        SpawnEnemies();
    }

    void SpawnEnemies()
    {
        source.Play();
        availableSpawns = new List<Transform>(spawnLocations);
        for (int i = 0; i < startingAmount && availableSpawns.Count > 0; i++)
        {
            Transform selectedLoc = availableSpawns[Random.Range(0, availableSpawns.Count)];
            var enemy = Instantiate(enemyPrefab, selectedLoc.position, Quaternion.identity);
            int extraHealth = roundCounter == 0 ? 0 : Mathf.RoundToInt(roundCounter * 6 * Mathf.Log(roundCounter) + 1);
            enemy.GetComponent<EnemyBrain>().SetMaxHealth(extraHealth);
            enemy.GetComponent<Damageable>().OnDeath.AddListener(() =>
            {
                spawnedEnemies.Remove(enemy);
                roundDisplay.text = spawnedEnemies.Count.ToString();
            });
            spawnedEnemies.Add(enemy);
            availableSpawns.Remove(selectedLoc);
        }
        roundCounter++;
        startingAmount = Mathf.RoundToInt(roundCounter * (Mathf.Log(roundCounter)) + 1);
    }

    private void Update()
    {
        if (spawnedEnemies.Count <= 2 && startingAmount > 3)
        {
            foreach (var enemy in spawnedEnemies)
            {
                enemy.GetComponent<EnemyBrain>().forceAttack = true;
                enemy.GetComponent<EnemyBrain>().SetState(EnemyBrain.EnemyState.Attacking);
            }
        }
        if (spawnedEnemies.Count == 0)
        {
            if (currentWait < waitBeforeNextRound)
            {
                currentWait += Time.deltaTime;
                roundDisplay.text = string.Empty;
                roundBarDisplay.fillAmount = currentWait / waitBeforeNextRound;
            }
            else
            {
                SpawnEnemies();
                roundDisplay.text = spawnedEnemies.Count.ToString();
                currentWait = 0;
            }
        }
    }
}