using UnityEngine;
using UnityEngine.Events;

public class OnInteract : MonoBehaviour, IInteract
{
    public UnityEvent onInteract;
    public void Interact(GameObject Player)
    {
        onInteract?.Invoke();
        Debug.Log("Interacted with " + gameObject.name);
    }
}